<?php

/**
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 *
 * @author Matthew Usurp <matt.usurp@gmail.com>
 * @copyright 2014 Matthew Usurp <matt.usurp@gmail.com>
 * @since 0.1.0
 */

namespace Gunship\Rei\File;

use Gunship;

/**
 * A constants interface to contain all directory pointers. A pointer requires that there are no directory separators,
 * instead the colon (:) should be used. These will be replaced at runtime.
 */
interface DirectoryInterface {

    //  the pointer that is used in place of a directory separator.
    const POINTER = ':';

    //  prefixes for the cache and manifest directories.
    const PREFIX_CACHE = '$cache';
    const PREFIX_MANIFEST = '$manifest';

    //  constants relating to the master top level directories.
    const MASTER_CACHE = 'rei.cache';
    const MASTER_MANIFEST = 'rei.manifest';

    //  constants referencing directories within the cache master.
    const CACHE_DOCTRINE_MODEL = '$cache:Doctrine:Model';
    const CACHE_DOCTRINE_PROXY = '$cache:Doctrine:Proxy';

    //  constants referencing directories within the manifest master.
    const MANIFEST_DATA = '$manifest:data';
    const MANIFEST_SCHEMA = '$manifest:schema';

}