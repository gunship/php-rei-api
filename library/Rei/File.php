<?php

/**
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 *
 * @author Matthew Usurp <matt.usurp@gmail.com>
 * @copyright 2014 Matthew Usurp <matt.usurp@gmail.com>
 * @since 0.1.0
 */

namespace Gunship\Rei;

use Gunship;
use Gunship\Rei\File\DirectoryInterface;

class File extends Gunship\Rei\Base implements DirectoryInterface {

    /** @var string */
    protected $root;

    //  --

    public function __construct($root) {
        $this->root = $root;
    }

    //  --

    public function getRootDirectory() {
        return $this->root;
    }

    public function getCacheDirectory() {
        return $this->getPointer(static::MASTER_CACHE);
    }

    public function getManifestDirectory() {
        return $this->getPointer(static::MASTER_MANIFEST);
    }

    //  --

    public function getPointer($constant) {
        if ( is_integer(strpos($constant, static::POINTER)) ) {
            $parts = explode(static::POINTER, $constant);

            switch ( array_shift($parts) ) {

                case static::PREFIX_CACHE:
                    return $this->getCacheDirectory() . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $parts);

                case static::PREFIX_MANIFEST:
                    return $this->getManifestDirectory() . DIRECTORY_SEPARATOR . implode(DIRECTORY_SEPARATOR, $parts);

                default:
                    throw new Gunship\Rei\Exception('Invalid pointer constant supplied');

            }

        } else {
            return $this->root . DIRECTORY_SEPARATOR . $constant;
        }
    }

}