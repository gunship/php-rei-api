<?php

/**
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 *
 * @author Matthew Usurp <matt.usurp@gmail.com>
 * @copyright 2014 Matthew Usurp <matt.usurp@gmail.com>
 * @since 0.1.0
 */

namespace Gunship\Rei;

use Gunship;

use Exception as StandardException;

/**
 * Standard level exception for the the {@link Gunship\Rei} package. All sub-exceptions should (and will) extend this,
 * meaning adding debug and additional functionality is easier.
 */
class Exception extends StandardException {}