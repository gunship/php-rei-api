<?php

/**
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 *
 * @author Matthew Usurp <matt.usurp@gmail.com>
 * @copyright 2014 Matthew Usurp <matt.usurp@gmail.com>
 * @since 0.1.0
 */

namespace Gunship\Rei;

use Gunship;

abstract class Base {

    /** @var Gunship\Rei */
    protected $rei;

    //  --

    public function setRei(Gunship\Rei $rei) {
        $this->rei = $rei;
    }

    //  --

    /**
     * Return the instance of {@link Gunship\Rei} this class belongs too.
     * @return Gunship\Rei
     */
    protected function getRei() {
        return $this->rei;
    }

}