<?php

/**
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 *
 * @author Matthew Usurp <matt.usurp@gmail.com>
 * @copyright 2014 Matthew Usurp <matt.usurp@gmail.com>
 * @since 0.1.0
 */

namespace Gunship\Rei;

use Gunship;

use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\YamlDriver;

class Schema extends Gunship\Rei\Base {

    /** @var EntityManager */
    protected $entity;

    //  --

    public function connect($connection) {
        $file = $this->getRei()->getFile();

        $this->getRei()->getAutoloader()->addPsr4('Rei\\Cache\\', $file->getCacheDirectory());

        $directory = $file->getPointer($file::CACHE_DOCTRINE_MODEL);
        $configuration = Setup::createAnnotationMetadataConfiguration(
            array($directory),
            true,
            $file->getPointer($file::CACHE_DOCTRINE_PROXY)
        );

        $this->entity = EntityManager::create($connection, $configuration);

    }

    //  --

    public function getEntityManager() {
        return $this->entity;
    }

    //  --

    public function getEntityName($entity) {
        return 'Rei\\Cache\\Doctrine\\Model\\' . $entity;
    }

}