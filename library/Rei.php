<?php

/**
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 *
 * @author Matthew Usurp <matt.usurp@gmail.com>
 * @copyright 2014 Matthew Usurp <matt.usurp@gmail.com>
 * @since 0.1.0
 */

namespace Gunship;

use Gunship;
use Gunship\Rei\File;
use Gunship\Rei\Schema;

use Composer;
use Composer\Autoload\ClassLoader;

final class Rei {

    /** @var Composer\Autoload\ClassLoader */
    protected $autoloader;

    /** @var Gunship\Rei\File */
    protected $file;

    /** @var Gunship\Rei\Schema */
    protected $schema;

    //  --

    public function __construct(ClassLoader $autoloader, $root, array $connection) {
        $this->autoloader = $autoloader;

        $this->file = new File($root);
        $this->file->setRei($this);

        $this->schema = new Schema;
        $this->schema->setRei($this);
        $this->schema->connect($connection);

    }

    //  --

    public function execute() {

        $e = $this->getSchema()->getEntityManager()->createQueryBuilder();
        $e->select(array('p.id', 'p.name'))->from($this->getSchema()->getEntityName('Product'), 'p');

        // @todo-debug ~ added on 08 July 2014
        echo '<pre><b>', __FILE__, ' (', __LINE__, ')</b> [', __METHOD__, ']', PHP_EOL;
        print_r(array(

            $e->getQuery()->getSQL()

        )); echo '</pre>', PHP_EOL; exit;

    }

    //  --

    /**
     * @return Composer\Autoload\ClassLoader
     */
    public function getAutoloader() {
        return $this->autoloader;
    }

    /**
     * @return Gunship\Rei\File
     */
    public function getFile() {
        return $this->file;
    }

    //  --

    /**
     * @return Gunship\Rei\Schema
     */
    public function getSchema() {
        return $this->schema;
    }

}