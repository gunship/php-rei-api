<?php

/**
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 *
 * @author Matthew Usurp <matt.usurp@gmail.com>
 * @copyright 2014 Matthew Usurp <matt.usurp@gmail.com>
 * @since 0.1.0
 */

namespace Gunship\Common;

use Gunship;

use ArrayAccess;

/**
 * A class that should convert array instances in to dictionary classes. The idea being that this should function
 * much like the Python dictionary type and implement most of the same methods. The {@link Gunship\Common\Dictionary}
 * also implemented the {@link ArrayAccess} interface so can use array notations (although not recommended).
 */
class Dictionary implements ArrayAccess {

    /** @var array */
    protected $data = array();

    //  --

    /**
     * On construction provide a $data array to pre-populate the dictionary object with.
     * @param array $data
     */
    public function __construct(array $data = array()) {
        foreach ( $data as $key => $value ) {
            $this->set($key, $value);
        }
    }

    //  --

    /**
     * Set a $key against the dictionary with the given $value.
     * @param string $key
     * @param mixed $value
     * @return void
     */
    public function set($key, $value) {
        $this->data[$key] = is_array($value)
            ? new static($value)
            : $value;
    }

    /**
     * Get a value from the dictionary with the given $key. Should the given $key not exist (uses {@link has()}) then
     * the $default parameter will be returned instead.
     * @param string $key
     * @param null|mixed $default
     * @return mixed|static
     */
    public function get($key, $default = null) {
        return $this->has($key)
            ? $this->data[$key]
            : $default;
    }

    /**
     * Return true if the given $key exists within the dictionary.
     * @param string $key
     * @return bool
     */
    public function has($key) {
        return isset($this->data[$key]);
    }

    /**
     * Remove the $key from the dictionary.
     * @param string $key
     */
    public function remove($key) {
        unset($this->data[$key]);
    }

    /**
     * Return all the keys set against the dictionary.
     * @return array
     */
    public function keys() {
        return array_keys($this->data);
    }

    /**
     * Return all the values set against the dictionary.
     * @return array
     */
    public function values() {
        return array_values($this->data);
    }

    /**
     * Return the length of the dictionary.
     * @return int
     */
    public function length() {
        return count($this->data);
    }

    /**
     * Return the internal data array behind the dictionary.
     * @return array
     */
    public function toArray() {
        $response = $this->data;

        foreach ( $response as $key => $value ) {
            if ( $value instanceof static ) {
                $response[$key] = $value->toArray();
            }
        }

        return $response;
    }

    //  --

    /**
     * Allow the setting of the given $key through the array notation.
     * @param string $key
     * @param mixed $value
     */
    public function offsetSet($key, $value) {
        $this->set($key, $value);
    }

    /**
     * Allow the getting of the given $key through the array notation.
     * @param string $key
     * @return mixed|static
     */
    public function offsetGet($key) {
        return $this->get($key);
    }

    /**
     * Allow the testing for the given $key through the array notation.
     * @param string $key
     * @return bool
     */
    public function offsetExists($key) {
        return $this->has($key);
    }

    /**
     * Allow the removal of the given $key through the array notation.
     * @param string $key
     */
    public function offsetUnset($key) {
        $this->remove($key);
    }

}