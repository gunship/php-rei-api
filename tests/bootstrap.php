<?php

/**
 * @author Matthew Usurp <matt.usurp@gmail.com>
 * @copyright 2014 Matthew Usurp <matt.usurp@gmail.com>
 * @since 0.1.0
 */

require_once dirname(__FILE__) . '/../vendor/autoload.php';