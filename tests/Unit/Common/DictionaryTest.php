<?php

/**
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 *
 * @author Matthew Usurp <matt.usurp@gmail.com>
 * @copyright 2014 Matthew Usurp <matt.usurp@gmail.com>
 * @since 0.1.0
 */

namespace Gunship\Tests\Common;

use Gunship;
use Gunship\Common\Dictionary;

/**
 * Basic test to cover the base {@link Gunship\Common\Dictionary} class.
 * @group common
 */
class DictionaryTest extends Gunship\Tests\Base\Unit {

    /**
     * Check functionality of the basic methods.
     * This should allow as close as possibly replication of the python dictionary type.
     */
    public function testDictionaryBasicFunctionality() {
        $dictionary = new Dictionary;

        //  testing that the internal data property is set to an array on construction.
        $this->assertCount(0, $dictionary->toArray(), 'Expected newly constructed dictionary to have an "array" with "0" entries');
        $this->assertEquals(0, $dictionary->length(), 'Expected newly constructed dictionary to have "0" entries');

        //  test the setting of values works as expected.
        $dictionary->set('name', 'Tony Stark');

        $this->assertCount(1, $dictionary->toArray(), 'Dictionary data should contain "1" entry');
        $this->assertEquals(1, $dictionary->length(), 'Dictionary length() method should return "1"');
        $this->assertTrue($dictionary->has('name'), 'Dictionary should have the "name" key');
        $this->assertEquals('Tony Stark', $dictionary->get('name'), 'Response should match what was set');

        //  test setting and getting a little more.
        $dictionary->set('material', 'Iron');
        $dictionary->set('universe', 'Marvel');

        $this->assertEquals(3, $dictionary->length(), 'Dictionary data should contain "3" entries');
        $this->assertEquals('Marvel', $dictionary->get('universe'), 'Response for "universe" should match what was set');

        //  test removal of a key.
        $dictionary->remove('material');

        $this->assertEquals(2, $dictionary->length(), 'Dictionary data should contain "2" entries after removal');
        $this->assertFalse($dictionary->has('material'), 'Dictionary should no longer have "material" as a key');

    }

    /**
     * Check the magic functionality of the class.
     * These magic methods should piggy back the above methods.
     */
    public function testMagicFunctionality() {

        //  construct dictionary with a given data array.
        $dictionary = new Dictionary(array(
            'name' => 'Tony Stark',
            'universe' => 'Marvel'
        ));

        //  test the internal data property again.
        $this->assertEquals(2, $dictionary->length(), 'Dictionary was constructed with data, expected "2" to be returned');

        //  attempt to set and get using the array notation.
        $dictionary['material'] = 'Iron';

        $this->assertEquals(3, $dictionary->length(), 'After using __set() the internal count should be "3"');
        $this->assertEquals('Iron', $dictionary['material'], 'Getting using __get() should return the expected value');

        //  testing the isset() magic functionality.
        $this->assertTrue(isset($dictionary['material']), 'Using isset() should return true when testing for something that exists');
        $this->assertFalse(isset($dictionary['company']), 'Using isset() should return false when testing a non-existent key');

        unset($dictionary['material']);
        $this->assertFalse(isset($dictionary['material']), 'Using isset() should return false after using unset() on the value');

    }

    /**
     * Check the various other utility methods work as expected.
     * These were included as they are part of the python dictionary listing.
     */
    public function testUtilityMethods() {
        $dictionary = new Dictionary;

        $dictionary->set('name', 'Tony Stark');
        $dictionary->set('universe', 'Marvel');

        //  test keys() and values() work as expected.
        $this->assertEquals(array('name', 'universe'), $dictionary->keys(), 'The keys() method should return all keys within the internal data');
        $this->assertEquals(array('Tony Stark', 'Marvel'), $dictionary->values(), 'The values() method should return all values within the internal data');

    }

    /**
     * Check that when setting an array (or series of arrays) that it instead becomes a dictionary.
     * This should not cause issues as a dictionary has array access.
     */
    public function testSubArrayBecomesDictionary() {

        //  the fixture data for this test.
        $fixture = array(
            'name' => 'Tony Stark',
            'suit' => array(
                'mark' => 1,
                'name' => 'Iron Man'
            )
        );

        //  construct a new instance of dictionary with the fixture data.
        //  this will also internally test the set() with array.
        $dictionary = new Dictionary($fixture);

        //  test the "suit" key has become an instance of dictionary.
        $this->assertInstanceOf('Gunship\\Common\\Dictionary', $dictionary->get('suit'), 'The "suit" key should have been transformed to a dictionary');
        $this->assertEquals('Iron Man', $dictionary->get('suit')->get('name'), 'Expected the "name" within "suit" to be what was set');

        //  check that getting the array returns what we expect.
        $this->assertEquals($fixture, $dictionary->toArray(), 'Expected the toArray() to kill all dictionary instances');

    }

}