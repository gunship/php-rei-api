#!/bin/sh

# scrutinizer should be ran on the cleanest stuff.
git clean -fd
git checkout .
git pull --all

# keep composer dependencies up-to-date for report.
composer install --dev

# run unit tests for clover report, then send with ocular.
vendor/bin/phpunit --coverage-clover=coverage.clover tests/Unit/
vendor/bin/ocular code-coverage:upload --format=php-clover coverage.clover