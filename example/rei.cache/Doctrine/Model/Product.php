<?php

/**
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 *
 * @author Matthew Usurp <matt.usurp@gmail.com>
 * @copyright 2014 Matthew Usurp <matt.usurp@gmail.com>
 * @since 0.1.0
 */

namespace Rei\Cache\Doctrine\Model;

/**
 * @Entity
 * @Table(name="product")
 */
class Product {

    /**
     * @Id
     * @GeneratedValue
     * @Column(type="integer")
     *
     * @var int
     */
    protected $id;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $hash;

    /**
     * @Column(type="string")
     * @var string
     */
    protected $name;

}