<?php

/**
 * For the full copyright and license information,
 * please view the LICENSE file that was distributed with this source code.
 *
 * @author Matthew Usurp <matt.usurp@gmail.com>
 * @copyright 2014 Matthew Usurp <matt.usurp@gmail.com>
 * @since 0.1.0
 */

/**
 * Require in the composer autoloader so all the required packages can load.
 * Note that this application has been built to be fully bootstrapped through composer.
 */
$autoloader = require_once dirname(__FILE__) . '/../../vendor/autoload.php';

//  the directory location that contains the "rei.*" directories.
$directory = realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..');

//  connection details.
$connection = array(
    'driver' => 'pdo_sqlite',
    'path' => $directory . DIRECTORY_SEPARATOR . 'db.sqlite'
);

//  instantiate a new Rei api instance and execute it.
$api = new Gunship\Rei($autoloader, $directory, $connection);
$api->execute();

// @todo-debug ~ added on 13 July 2014
echo '<pre><b>', __FILE__, ' (', __LINE__, ')</b> [', __METHOD__, ']', PHP_EOL;
print_r(array($api)); echo '</pre>', PHP_EOL; exit;