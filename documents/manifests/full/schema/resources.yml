# this file contains all resources to be used with the API.

# list all resources that will be available.
resources:

  # each resource requires the following:

  # - label
  # - $table

  # define a label for the resource, this is what will be used for "$" references.
  - label: artist

    # by default this is true and therefore can be omitted.
    # only required if a resource has been marked not for use with an envelope.
    active: false

    # the $table and $column(s) that this resource will make use of.
    # note the "$" means a reference to an entity within these documents.
    $table: artist

    # omitting this will mean all keys that are not marked as internal will be shown.
    # compound resources need to be thought about a little more.
    $columns: [id, hash, title]

    # inspired by ActiveRecord (http://guides.rubyonrails.org/association_basics.html).
    relationships:

      # each relationship requires the following setup:

      # - "type" defining the type of the join.
      #   format: string one of the below types.

        # - belongs-to:
        #     foreign key is within this table.
        #     will target a resource so will also return one.

        # - has-one:
        #     foreign key is within other table.
        #     will target a resource so will also return one.
        #     can through a table.

        # - has-many:
        #     foreign key is within other table.
        #     will return a collection.
        #     can through a table.

        # - many-many:
        #     will return a collection.
        #     foreign keys are both in a through table.

      # - "join" defining the join the relationship will make.
      #   format: { target: n, $column: n } with the above definitions where required.
      #     "target" is dynamic depending on the join type.

      # - "through" defines a table to go through.
      #   format: { $table: n, origin: n, destination: n }

      # - "expanded" if this should be expanded and when its acceptable to do so.
      #   format: either of te below
      #     - "auto" expand if its parent is a primary resource, or was requested through "expand".
      #     - "manual" expand only when requested through "expand".
      #     - "true" always expand this.
      #     - "false" never expand this even when requested, good for showing meta links (HAL example)
      #   default: auto

      # an example of the "artist" resource having a collection of "albums".
      - type: has-many
        join: { target: albums, $column: artist_id }
        expanded: manual

      # writing examples using the examples on (http://guides.rubyonrails.org/association_basics.html).

      # an order belonging to a customer, where this is now an order resource.
      # targeting the customer resource using the "customer_id" column within the order resource.
      - type: belongs-to
        join: { target: customer, $column: customer_id }

      # a supplier has one account, where this is now a supplier resource.
      # targeting the account resource using the "supplier_id" column within the account resource.
      - type: has-one
        join: { target: account, $column: supplier_id }

      # a supplier has one account_history linked to an account, where this is now a supplier resource.
      # targeting an account_history through the account resource, where primary keys match the defined.
      - type: has-one
        join: { target: account_history }
        through: { target: account, origin: supplier_id, destination: account_id }

      # a customer resource may have many orders against it, where this is now a customer resource.
      # targeting the orders collection using the "customer_id" within the order resource.
      - type: has-many
        join: { target: orders, $column: customer_id }

      # a physician has many patients through an appointments table, where this is now a physician resource.
      # targeting a collection of patient through an appointment table, where primary keys match the defined.
      - type: has-many
        join: { target: patients }
        through: { target: appointment, origin: physician_id, destination: patient_id }

      # a assembly will have many parts, a part will belong to many assemblies, assuming this is a assembly resource.
      # targeting a collection of parts through a assembly_part relationship table.
      - type: many-many
        join: { target: part }
        through: { target: assembly_part, origin: assembly_id, destination: part_id }